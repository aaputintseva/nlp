package machineLearning;

import myAlgorithm.Sentence;
import myAlgorithm.Word;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

public final class Utils {

    private Utils() {}

    public static double calculateAverageDistanceBetweenElidedWordAndHead(String filename) throws IOException, URISyntaxException {

        SentenceReader reader = new SentenceReader();
        ArrayList<Sentence> sentences = reader.readAsSentenceList(filename);

        double averageDistance = 0;
        int elidedWordsCount = 0;

        for (Sentence sentence : sentences) {
            for (int i = 0; i < sentence.getValue().size(); i++) {
                Word word = sentence.getValue().get(i);
                if (word.getId().contains(".")) {
                    String[] deps = word.getDeps().split("\\:");
                    if (!deps[1].equals("exroot") && !deps[0].contains(".")) {
                        try {
                            averageDistance += Math.abs(Integer.valueOf(deps[0]) - Math.ceil(Double.valueOf(word.getId())));
                            elidedWordsCount++;
                        }
                        catch (RuntimeException e) {
                            System.out.print(word + " | ");
                            System.out.println(Arrays.toString(deps));
                        }
                    }
                }
            }
        }


        return averageDistance / elidedWordsCount;
    }
}
