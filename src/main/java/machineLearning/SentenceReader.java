package machineLearning;

import myAlgorithm.Demo;
import myAlgorithm.Sentence;
import myAlgorithm.Word;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SentenceReader {

    /**
     * Reads data from CoNLL-resource (.conll, .conllu)
     *
     * @param filename File name
     * @return Data in form of a ArrayList of String
     * @throws IOException
     * @throws URISyntaxException
     */
    List<String> readAsStringList(String filename) throws IOException, URISyntaxException {

        URL resource = Demo.class.getResource(filename);
        if (resource == null) {
            throw new NullPointerException();
        }

        List<String> lines = Files.readAllLines(Paths.get(resource.toURI()));

        return lines;
    }

    ArrayList<Word> readAsWordList(String filename) throws IOException, URISyntaxException {

        List<String> rawLines = readAsStringList(filename);

        ArrayList<Word> corpus = new ArrayList<>();
        for (String line : rawLines) {
            if (!line.startsWith("#") && !line.isEmpty()) {
                Word word = new Word(line);
                corpus.add(word);
            }
        }
        return corpus;
    }

    ArrayList<Sentence> readAsSentenceList(String filename) throws IOException, URISyntaxException {

        List<String> rawLines = readAsStringList(filename);

        ArrayList<Sentence> corpus = new ArrayList<>();
        Sentence sentence = null;

        for (String line : rawLines) {

            if (!line.startsWith("#") && !line.isEmpty()) {
                Word w = new Word(line);
                if (sentence == null) {
                    sentence = new Sentence(w); // Add word to a new sentence
                } else {
                    sentence.addWord(w); // Add word to existing sentence
                }
            }

            if (line.isEmpty() && sentence != null) {
                corpus.add(sentence); // Add sentence to the corpus
                sentence = null;
            }
        }

        if (sentence != null) { // If the file doesn't end with an empty/comment line
            corpus.add(sentence);
        }

        return corpus;
    }
}
