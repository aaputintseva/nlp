package machineLearning;

import myAlgorithm.Sentence;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class Demo {

    public static void main(String[] args) throws IOException, URISyntaxException {
        //makeTable();

        System.out.println(Utils.calculateAverageDistanceBetweenElidedWordAndHead("/ru_syntagrus-ud-train.conllu"));
    }

    private static void makeTable() throws IOException, URISyntaxException {
        // Reading data from text
        SentenceReader reader = new SentenceReader();
        ArrayList<Sentence> sentences = reader.readAsSentenceList("/0005.conll");

        // Objects for writing to CSV-file
        PrintWriter pw = new PrintWriter(new File("table.csv"));
        StringBuilder sb = new StringBuilder();

        // Adding headers to CSV
        sb.append("Sentence,N-ellipsis distance,Repetition distance\n");

        for (Sentence sentence : sentences) {

            // Adding a sentence to CSV
            sb.append(sentence.concatWordForms());
            sb.append(',');

            // Adding features
            FeatureGetter fg = new FeatureGetter();

            // 1. N-ellipsis feature
            sb.append(fg.getNounEllipsisFeature(sentence.getValue(), 5));
            sb.append(',');

            // 2. Repetition feature
            sb.append(fg.getRepetitionFeature(sentence.getValue()));
            sb.append('\n');
        }

        pw.write(sb.toString());
        pw.flush();
        pw.close();
    }
}
