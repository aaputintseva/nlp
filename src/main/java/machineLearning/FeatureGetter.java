package machineLearning;

import myAlgorithm.Word;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class FeatureGetter {

    /**
     * Uses rules from table in ML Ellipsis
     *
     * @param words
     * @param window
     * @return Average distance
     * @throws IOException
     * @throws URISyntaxException
     */
    public double getNounEllipsisFeature(ArrayList<Word> words, int window) {
        // Infinity - ellipsis

        ArrayList<Double> distances = new ArrayList<>(); // Distances for all ADJ/DET/NUM in sentence

        for (int i = 0; i < words.size(); i++) {

            Word currentWord = words.get(i);

            // Current word must be ADJ (full form), DET or NUM
            if (!(currentWord.getUpostag().equals("ADJ")
                    || currentWord.getUpostag().equals("DET")
                    || currentWord.getUpostag().equals("NUM"))
                    || currentWord.getVariant().equals("Short")) {
                continue;
            }

            int beginIndex = (i - window >= 0) ? i - window : 0; // Index of window start
            double minDistance = Double.POSITIVE_INFINITY;

            if (!currentWord.getUpostag().equals("NUM")) { // Rules for ADJ and DET
                for (int j = beginIndex; j <= i + window && j < words.size(); j++) { // Window analysis
                    if (i != j) {
                        if (words.get(j).isPosTagIn("NOUN", "PROPN", "PRON")
                                && currentWord.equalsInFeatures(words.get(j), new String[]{"Gender", "Number", "Case"})
                                && Math.abs(i - j) < minDistance
                                || words.get(j).getUpostag().equals("NUM") && Math.abs(i - j) < minDistance) {
                            minDistance = Math.abs(i - j);
                        }
                    }
                }
            } else {
                for (int j = beginIndex; j <= i + window && j < words.size(); j++) { // Window analysis
                    if (i != j) {
                        if (words.get(j).isPosTagIn("NOUN", "PROPN", "PRON")
                                && currentWord.equalsInFeatures(words.get(j), new String[]{"Gender", "Case"})
                                && Math.abs(i - j) < minDistance) {
                            minDistance = Math.abs(i - j);
                        }
                        if (words.get(j).getUpostag().equals("ADJ") && Math.abs(i - j) < minDistance) {
                            minDistance = Math.abs(i - j);
                        }
                    }
                }
            }

            distances.add(minDistance);

        }

        if (distances.isEmpty()) {
            return 0;
        }

        double averageDistance = 0;

        for (Double distance : distances) {
            averageDistance += distance;
        }

        averageDistance /= distances.size();

        return averageDistance;
    }

    public double getRepetitionFeature(ArrayList<Word> words) {
        // Not infinity - may be no ellipsis (just repetition)

        ArrayList<Double> distances = new ArrayList<>();

        for (int i = 0; i < words.size(); i++) {

            Word currentWord = words.get(i);

            // Current word must be ADJ (full form), DET or NUM
            if (!(currentWord.getUpostag().equals("ADJ")
                    || currentWord.getUpostag().equals("DET")
                    || currentWord.getUpostag().equals("NUM"))
                    || currentWord.getVariant().equals("Short")) {
                continue;
            }

            double minDistance = Double.POSITIVE_INFINITY;

            for (int j = 0; j < words.size(); j++) {
                if (i != j) {
                    if (
                        // One of the forms starts with the other form
                            (currentWord.getForm().startsWith(words.get(j).getForm())
                                    || words.get(j).getForm().startsWith(currentWord.getForm()))
                                    &&
                                    // One of the POS-tags is X or they are equal
                                    (currentWord.getUpostag().equals("X")
                                            || words.get(j).getUpostag().equals("X")
                                            || currentWord.getUpostag().equals(words.get(j).getUpostag()))
                                    &&
                                    // This word is the nearest word
                                    Math.abs(i - j) < minDistance) {
                        minDistance = Math.abs(i - j);
                    }
                }
            }

            if (Math.abs(minDistance - Double.POSITIVE_INFINITY) > 1e-3) {
                distances.add(minDistance);
            }
        }

        if (distances.isEmpty()) {
            return Double.POSITIVE_INFINITY;
        }

        double averageDistance = 0;

        for (Double distance : distances) {
            averageDistance += distance;
        }

        averageDistance /= distances.size();

        return averageDistance;
    }
}
