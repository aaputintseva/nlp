package myAlgorithm;

public class Triple {
    private Word head;
    private String rel;
    private Word dep;

    public Triple(Word head, String rel, Word dep) {
        this.head = head;
        this.rel = rel;
        this.dep = dep;
    }

    @Override
    public String toString() {

        String res = "";
        res += (head != null)? head.getUpostag() + " " : "null ";
        res += (rel != null)? rel + " " : "null ";
        res += (dep != null)? dep.getUpostag() : "null";

        return res;
    }

    public Word getHead() {
        return head;
    }

    public void setHead(Word head) {
        this.head = head;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public Word getDep() {
        return dep;
    }

    public void setDep(Word dep) {
        this.dep = dep;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triple triple = (Triple) o;

        if (head != null ? !head.equals(triple.head) : triple.head != null) return false;
        if (!rel.equals(triple.rel)) return false;
        return dep.equals(triple.dep);
    }

    @Override
    public int hashCode() {
        int result = head != null ? head.hashCode() : 0;
        result = 31 * result + rel.hashCode();
        result = 31 * result + dep.hashCode();
        return result;
    }
}
