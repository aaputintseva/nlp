package myAlgorithm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;

public class Utils {

    /**
     * Utility method for reading data from CoNLL-resource (.conll, .conllu)
     *
     * @param filename File name
     * @return Data in form of a ArrayList of String
     * @throws IOException
     * @throws URISyntaxException
     */
    public static List<String> readAsList(String filename) throws IOException, URISyntaxException {

        URL resource = Demo.class.getResource(filename);
        if (resource == null) {
            throw new NullPointerException();
        }

        List<String> lines = Files.readAllLines(Paths.get(resource.toURI()));

        return lines;
    }

    /**
     * Reads data from CoNLL-resource (.conll, .conllu)
     *
     * @param filename File name
     * @return Data in form of an ArrayList of Sentence
     * @throws IOException
     * @throws URISyntaxException
     */
    public static ArrayList<Sentence> readAsSentenceList(String filename) throws IOException, URISyntaxException {

        List<String> rawLines = readAsList(filename);

        ArrayList<Sentence> corpus = new ArrayList<>();
        Sentence sentence = null;

        for (String line : rawLines) {

            if (!line.startsWith("#") && !line.isEmpty()) {
                Word w = new Word(line);
                if (sentence == null) {
                    sentence = new Sentence(w); // Add word to a new sentence
                } else {
                    sentence.addWord(w); // Add word to existing sentence
                }
            }

            if (line.isEmpty() && sentence != null) {
                corpus.add(sentence); // Add sentence to the corpus
                sentence = null;
            }
        }

        if (sentence != null) { // If the file doesn't end with an empty/comment line
            corpus.add(sentence);
        }

        return corpus;
    }

    /**
     * Prints HashMap to the console
     *
     * @param hm Printing HashMap object
     */
    public static void printHashMap(HashMap hm) {
        for (Object key : hm.keySet()) {
            System.out.println(key + ": " + hm.get(key));
        }
    }

    /**
     * Gets dependency statistics from corpus
     *
     * @param corpus Annotated text corpus
     * @return HashMap containing triples as keys and appropriate relative frequencies as values
     */
    public static HashMap<Triple, Double> getStatistics(ArrayList<Sentence> corpus) {

        HashMap<Triple, Double> statistics = new HashMap<>();
        int triplesCount = 0;

        for (Sentence sentence : corpus) {
            for (Word word : sentence.getValue()) {

                // Create triple from current word
                Word head = null;
                if (!word.getHead().equals("0") && !word.getHead().equals("_")) {
                    head = sentence.getValue().get(Integer.valueOf(word.getHead()) - 1);
                }
                Triple triple = new Triple(head, word.getDeprel(), word);

                // Add triple to HashMap
                if (statistics.containsKey(triple)) {
                    statistics.put(triple, statistics.get(triple) + 1);
                } else {
                    statistics.put(triple, (double) 1);
                }

                // Method 2
//                statistics.computeIfPresent(triple, (k, v) -> v + 1);
//                statistics.computeIfAbsent(triple, (v) -> 1);

                //Method 3
//                statistics.compute(triple, (k, v) -> v == null ? 1 : v + 1)
            }

            triplesCount += sentence.getValue().size();
        }

        int count = triplesCount;

        for (Triple triple : statistics.keySet()) {
            statistics.compute(triple, (k, v) -> v / count); // Computing relative frequencies
        }

        return statistics;
    }

    public static HashMap<Triple, Double> searchByHead(HashMap<Triple, Double> data, String head) {

        HashMap<Triple, Double> res = new HashMap<>();

        for (Map.Entry<Triple, Double> entry : data.entrySet()) {
            if (entry.getKey().getHead() != null && entry.getKey().getHead().getUpostag().equals(head)) {
                res.put(entry.getKey(), entry.getValue());
            }
        }

        return res;
    }

    public static HashMap<Triple, Double> searchByHeadAndRel(HashMap<Triple, Double> data,
                                                             HashMap<Triple, Double> statistics,
                                                             String head,
                                                             String rel) {

        HashMap<Triple, Double> res = new HashMap<>();

        for (Map.Entry<Triple, Double> entry : data.entrySet()) {

            if (entry.getKey().getHead() != null && entry.getKey().getRel() != null
                    && entry.getKey().getHead().getUpostag().equals(head) && entry.getKey().getRel().equals(rel)) {
                res.put(entry.getKey(), statistics.get(entry.getKey()));
            }

        }

        return res;
    }

    public static HashMap<Triple, Double> searchByHeadAndDep(HashMap<Triple, Double> data,
                                                             HashMap<Triple, Double> statistics,
                                                             String head,
                                                             String dep) {

        HashMap<Triple, Double> res = new HashMap<>();

        for (Map.Entry<Triple, Double> entry : data.entrySet()) {

            if (entry.getKey().getHead() != null &&
                    entry.getKey().getDep() != null &&
                    entry.getKey().getHead().getUpostag().equals(head) &&
                    entry.getKey().getDep().getUpostag().equals(dep)) {
                res.put(entry.getKey(), statistics.get(entry.getKey()));
            }

        }

        return res;
    }

    public static HashMap<Triple, Double> getUncommon(ArrayList<Sentence> data, ArrayList<Sentence> corpus,
                                                      double boundary) {

        HashMap<Triple, Double> res = new HashMap<>();

        Map<Triple, Double> analyzedData = getStatistics(data);
        Map<Triple, Double> statistics = getStatistics(corpus);

        for (Map.Entry<Triple, Double> entry : analyzedData.entrySet()) {

            if (!statistics.containsKey(entry.getKey()) || statistics.get(entry.getKey()) < boundary) {
                res.put(entry.getKey(), statistics.get(entry.getKey()));
            }

        }

        return res;
    }

    // head - rel - dependent

    public static void resolveEllipsis(ArrayList<Sentence> text, ArrayList<Sentence> corpus) {
        HashMap<Triple, Double> uncommon = getUncommon(text, corpus, 1E-3);

        HashMap<Triple, Double> textStatistics = getStatistics(text);
        HashMap<Triple, Double> corpusStatistics = getStatistics(corpus);

        for (Entry<Triple, Double> uncommonEntry : uncommon.entrySet()) { // For every uncommon triple

            System.out.println();

            // Step 1. Looking for elided element's POS-tag

            HashMap<Triple, Double> foundPOS = searchByHeadAndRel(
                    textStatistics,
                    corpusStatistics,
                    uncommonEntry.getKey().getHead().getUpostag(),
                    uncommonEntry.getKey().getRel());

            System.out.println("POS options:");
            printHashMap(foundPOS);
            System.out.println();

            // Entry with max relative frequency
            Entry<Triple, Double> guessUpostag = uncommonEntry;
            for (Entry<Triple, Double> optionEntry : foundPOS.entrySet()) {
                if (optionEntry.getValue() > guessUpostag.getValue()) {
                    guessUpostag = optionEntry;
                }
            }

            System.out.println("Guess: " + guessUpostag.getKey() + ": " + guessUpostag.getValue() + "\n");

            // Step 2. Looking for relation between elided element and it's dependent

            HashMap<Triple, Double> foundRel = searchByHeadAndDep(
                    textStatistics,
                    corpusStatistics,
                    guessUpostag.getKey().getDep().getUpostag(),
                    uncommonEntry.getKey().getDep().getUpostag());

            System.out.println("Rel options:");
            printHashMap(foundRel);
            System.out.println();

            // Entry with max relative frequency
            Entry<Triple, Double> guessRel = foundRel.entrySet().iterator().next();
            for (Entry<Triple, Double> optionEntry : foundRel.entrySet()) {
                if (optionEntry.getValue() > guessRel.getValue()) {
                    guessRel = optionEntry;
                }
            }

            System.out.println("Guess: " + guessRel.getKey() + ": " + guessRel.getValue());
        }
    }
}
