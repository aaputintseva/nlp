package myAlgorithm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import static myAlgorithm.Utils.*;

public class Demo {

    public static void main(String[] args) throws IOException, URISyntaxException {

//        ArrayList<Sentence> text = readAsSentenceList("/test.conll");
//
//        System.out.println("Analyzed sentences:");
//        for (Sentence sentence : text) {
//            System.out.println(sentence);
//            System.out.println();
//        }
//
//        ArrayList<Sentence> corpus = readAsSentenceList("/ru_syntagrus-ud-train.conllu");

//        System.out.println("Statistical data:");
//        for (Sentence sentence : corpus) {
//            System.out.println(sentence);
//            System.out.println();
//        }

//        HashMap<Triple, Double> statistics = getStatistics(corpus);

//        System.out.println("Statistics:");
//        printHashMap(statistics);

//        HashMap<Triple, Double> searched1 = searchByHead(statistics, "VERB");
//        System.out.println("Search by head:");
//        printHashMap(searched1);
//        System.out.println();
//
//        HashMap<Triple, Double> searched2 = searchByHeadAndRel(statistics, "VERB", "obj");
//        System.out.println("Search by head and rel:");
//        printHashMap(searched2);
//        System.out.println();
//
//        System.out.println("Uncommon triples:");
//        HashMap<Triple, Double> uncommon = getUncommon(text, statistics, 1E-3);
//        printHashMap(uncommon);
//        System.out.println();
//
//        resolveEllipsis(text, statistics);


        ArrayList<Sentence> analyzing = readAsSentenceList("/test.conll");
        ArrayList<Sentence> corpus = readAsSentenceList("/ru_syntagrus-ud-train.conllu");

        resolveEllipsis(analyzing, corpus);

    }
}