package myAlgorithm;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Sentence {

    private ArrayList<Word> value;

    public Sentence(ArrayList<Word> value) {
        this.value = value;
    }

    public Sentence(Word word) {
        this.value = new ArrayList<Word>();
        this.value.add(word);
    }

    public ArrayList<Word> getValue() {
        return value;
    }

    public void setValue(ArrayList<Word> value) {
        this.value = value;
    }

    public void addWord(Word word) {
        value.add(word);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Word w : value) {
            if (sb.length() != 0) {
                sb.append('\n');
            }
            sb.append(w.toString());
        }
        return sb.toString();
    }

    public String concatWordForms() {
        return String.join(" ", value.stream().map(Word::getForm).collect(Collectors.toList()));
    }
}
