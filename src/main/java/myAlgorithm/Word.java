package myAlgorithm;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Keeps CoNLL line
 */
public class Word {

    /**
     * Word index.
     * may be an integer starting at 1 for each new sentence;
     * may be a range for multiword tokens;
     * may be a decimal number for empty nodes.
     */
    private String id;

    /**
     * Word form or punctuation symbol.
     */
    private String form;

    /**
     * Lemma or stem of word form.
     */
    private String lemma;

    /**
     * Universal part-of-speech tag.
     */
    private String upostag;

    /**
     * Language-specific part-of-speech tag; underscore if not available.
     */
    private String xpostag;

    /**
     * List of morphological features from the universal feature inventory or from a defined language-specific
     * extension; underscore if not available.
     */
    private Map<String, String> featsProps;

    /**
     * Head of the current word, which is either a value of ID or zero (0).
     */
    private String head;

    /**
     * Universal dependency relation to the HEAD (root if HEAD = 0) or a defined language-specific subtype of one.
     */
    private String deprel;

    /**
     * Enhanced dependency graph in the form of a list of head-deprel pairs.
     */
    private String deps;

    /**
     * Any other annotation.
     */
    private String misc;

    public Word(String line) {

        if (line.startsWith("#") || line.isEmpty()) {
            throw new RuntimeException("Line is empty or comment");
        }

        String[] lineAsArr = line.split("\\t");

        this.id = lineAsArr[0];
        this.form = lineAsArr[1];
        this.lemma = lineAsArr[2];
        this.upostag = lineAsArr[3];
        this.xpostag = lineAsArr[4];
        this.featsProps = initializeFeats(lineAsArr[5]);
        this.head = lineAsArr[6];
        this.deprel = lineAsArr[7];
        this.deps = lineAsArr[8];
        this.misc = lineAsArr[9];
    }

    private static Map<String, String> initializeFeats(String s) {
        Map<String, String> result = new LinkedHashMap<>();
        if (!"_".equals(s)) {
            String[] featsArray = s.split("\\|");
            for (String pair : featsArray) {
                String[] p = pair.split("=");
                result.put(p[0], p[1]);
            }
        }
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getUpostag() {
        return upostag;
    }

    public void setUpostag(String upostag) {
        this.upostag = upostag;
    }

    public String getXpostag() {
        return xpostag;
    }

    public void setXpostag(String xpostag) {
        this.xpostag = xpostag;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getDeprel() {
        return deprel;
    }

    public void setDeprel(String deprel) {
        this.deprel = deprel;
    }

    public String getDeps() {
        return deps;
    }

    public void setDeps(String deps) {
        this.deps = deps;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    @Override
    public String toString() {
        return id + '\t' + form + '\t' + lemma + '\t' + upostag + '\t' + xpostag + '\t' + printFeats(featsProps) + '\t' + head + '\t' +
                deprel + '\t' + deps + '\t' + misc;
    }

    private static String printFeats(Map<String, String> featsProps) {
        return featsProps.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("|"));
//
//        StringJoiner sj = new StringJoiner("|");
//        for (Map.Entry<String, String> e : featsProps.entrySet()) {
//            sj.add(e.getKey() + "=" + e.getValue());
//        }
//        return sj.toString();
    }

    /**
     * Uses only upostag, because it is necessary for Triple.equals(o)
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        return upostag != null ? upostag.equals(word.upostag) : word.upostag == null;
    }

    @Override
    public int hashCode() {
        return upostag != null ? upostag.hashCode() : 0;
    }

    /**
     * Returns feature value
     *
     * @param featureName
     * @return
     */
    private String getFeature(String featureName) {
        return this.featsProps.getOrDefault(featureName, "");
    }

    public String getVariant() {
        return getFeature("Variant");
    }

    public boolean containsFeats() {
        return !this.featsProps.isEmpty();
    }

    private String getFeat(String name) {
        return this.featsProps.get(name);
    }

    /**
     * @param word
     * @param featuresNames
     * @return true if all parameters existing are equals
     */
    public boolean equalsInFeatures(Word word, String[] featuresNames) {

        if (!containsFeats() || !word.containsFeats()) {
            return true;
        }

        for (String featureName : featuresNames) {
            String thisFeat = getFeat(featureName);
            String thatFeat = word.getFeat(featureName);

            if (thisFeat == null || thatFeat == null) {
                continue;
            }

            if (!thisFeat.equals(thatFeat)) {
                return false;
            }
        }
        return true;
    }

    public boolean equalsInFeatures(Word word) {

        if (!containsFeats() || !word.containsFeats()) {
            return true;
        }

        String[] featureNames = this.featsProps.keySet().toArray(new String[0]);
        return equalsInFeatures(word, featureNames);
    }

    public boolean isPosTagIn(String... tags) {
        for (String tag : tags) {
            if (upostag.equals(tag)) {
                return true;
            }
        }
        return false;
    }
}
